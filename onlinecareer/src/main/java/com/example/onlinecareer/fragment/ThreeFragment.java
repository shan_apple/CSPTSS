package com.example.onlinecareer.fragment;


import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.onlinecareer.R;
import com.example.onlinecareer.activities.AboutActivity;
import com.example.onlinecareer.activities.PersonActivity;
import com.example.onlinecareer.util.CommonData;

@SuppressLint("NewApi")
public class ThreeFragment extends Fragment implements OnClickListener {

	private View rootView;

	private TextView tv_person, tv_about, tv_loginout, tv_nickName;
	private ImageView iv_head;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater.inflate(R.layout.fragment_three, container,
					false);
		}
		return rootView;
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initView();

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (CommonData.isLogin)
			tv_nickName.setText(CommonData.user_hashMap.get("name").toString());
	}

	private void initView() {
		tv_person = (TextView) rootView.findViewById(R.id.tv_person);
		tv_about = (TextView) rootView.findViewById(R.id.tv_about);
		tv_loginout = (TextView) rootView.findViewById(R.id.tv_loginout);
		tv_nickName = (TextView) rootView.findViewById(R.id.tv_nickName);
		iv_head=(ImageView)rootView.findViewById(R.id.iv_head);

		tv_person.setOnClickListener(this);
		tv_about.setOnClickListener(this);
		tv_loginout.setOnClickListener(this);
		iv_head.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_head:
			if(CommonData.isLogin){
				Intent intent = new Intent(getActivity(), PersonActivity.class);
				startActivity(intent);
			}else{
				Toast.makeText(getActivity(), "�û���δ��¼��", Toast.LENGTH_SHORT).show();
			}		
			break;
		case R.id.tv_person:
			if(CommonData.isLogin){
				Intent intent = new Intent(getActivity(), PersonActivity.class);
				startActivity(intent);
			}else{
				Toast.makeText(getActivity(), "�û���δ��¼��", Toast.LENGTH_SHORT).show();
			}	
			break;
		case R.id.tv_about:
			Intent intent = new Intent(getActivity(), AboutActivity.class);
			startActivity(intent);
			break;
		case R.id.tv_loginout:
			CommonData.isLogin = false;
			CommonData.user_hashMap=null;
			tv_nickName.setText("δ��¼");
			Toast.makeText(getActivity(), "�˳���¼��", Toast.LENGTH_SHORT).show();
			break;
		}
	}

}