package com.example.onlinecareer.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.example.onlinecareer.R;
import com.example.onlinecareer.adapter.JobAdapter;

@SuppressLint("NewApi")
public class TwoFragment extends Fragment implements OnClickListener {

	private View rootView;

	private TextView tv_start, tv_going, tv_over;
	private ListView lv_task;
	private JobAdapter adapter;
    private List<HashMap<String, Object>> listData;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (rootView == null) {
			rootView = inflater
					.inflate(R.layout.fragment_two, container, false);
		}
		return rootView;
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initData();
		initView();
		
		lv_task.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {	
			}
		});
	}
	
	private void initData(){
    	listData=new ArrayList<HashMap<String,Object>>();
    }

	private void initView() {
		tv_start = (TextView) rootView.findViewById(R.id.tv_start);
		tv_going = (TextView) rootView.findViewById(R.id.tv_going);
		tv_over = (TextView) rootView.findViewById(R.id.tv_over);
		lv_task = (ListView) rootView.findViewById(R.id.lv_task);
		
		tv_start.setOnClickListener(this);
		tv_going.setOnClickListener(this);
		tv_over.setOnClickListener(this);
		
		adapter=new JobAdapter(getActivity(), listData);
		lv_task.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		setTextColor();
		switch (v.getId()) {
		case R.id.tv_start:
			tv_start.setTextColor(getResources().getColor(R.color.basecolor));
			break;
		case R.id.tv_going:
			tv_going.setTextColor(getResources().getColor(R.color.basecolor));
			break;
		case R.id.tv_over:
			tv_over.setTextColor(getResources().getColor(R.color.basecolor));
			break;
		}
	}
	
	//�ı�����ı�����ɫ
		public void setTextColor() {
			tv_start.setTextColor(getResources().getColor(R.color.text_color_primary));
			tv_going.setTextColor(getResources().getColor(R.color.text_color_primary));
			tv_over.setTextColor(getResources().getColor(R.color.text_color_primary));
		}

}