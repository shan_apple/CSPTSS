package com.example.onlinecareer.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.example.onlinecareer.R;
import com.example.onlinecareer.activities.JobDetailActivity;
import com.example.onlinecareer.adapter.JobAdapter;
import com.example.onlinecareer.util.CommonData;

@SuppressLint("NewApi")
public class OneFragment extends Fragment implements OnClickListener{

    private View rootView;
    
    private TextView tv_part_job,tv_full_time;
    private ListView lv_job;
    private JobAdapter adapter;
    private List<HashMap<String, Object>> part_job_Data;
    private List<HashMap<String, Object>> full_job_Data;
    private int job_type=0;//0�����ְ��1����ȫְ
    
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_one, container, false);
        }
        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        initData();
        initView();
        
        //����ְλ����
        lv_job.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int position,
					long arg3) {
				Intent intent=new Intent(getActivity(), JobDetailActivity.class);
				Bundle data = new Bundle();
				data.putSerializable("job", (HashMap<String, Object>)adapter.getItemAtPosition(position));
				intent.putExtras(data);
				startActivity(intent);
			}
		});
    }

    private void initData(){
    	part_job_Data=new ArrayList<HashMap<String, Object>>();
    	full_job_Data=new ArrayList<HashMap<String, Object>>();
    	for (int i = 0; i < CommonData.part_job_name.length; i++) {
    		HashMap<String, Object> hashMap=new HashMap<String, Object>(); 
    		hashMap.put("img",CommonData.part_job_img[i]);
    		hashMap.put("name",CommonData.part_job_name[i]);
    		hashMap.put("jobType","��ְ");
    		hashMap.put("company",CommonData.part_job_company[i]);
    		hashMap.put("time",CommonData.part_job_time[i]);
    		hashMap.put("salary",CommonData.part_job_salary[i]);
    		hashMap.put("people",CommonData.part_job_people[i]);
    		hashMap.put("address",CommonData.part_job_address[i]);
    		hashMap.put("payWay",CommonData.part_job_payway[i]);  
    		part_job_Data.add(hashMap);
		}
    	for (int i = 0; i < CommonData.full_job_name.length; i++) {
    		HashMap<String, Object> hashMap=new HashMap<String, Object>(); 
    		hashMap.put("img",CommonData.full_job_img[i]);
    		hashMap.put("name",CommonData.full_job_name[i]);
    		hashMap.put("jobType","ȫְ");
    		hashMap.put("company",CommonData.full_job_company[i]);
    		hashMap.put("time",CommonData.full_job_time[i]);
    		hashMap.put("salary",CommonData.full_job_salary[i]);
    		hashMap.put("people",CommonData.full_job_people[i]);
    		hashMap.put("address",CommonData.full_job_address[i]);
    		hashMap.put("payWay",CommonData.full_job_payway[i]);  
    		full_job_Data.add(hashMap);
    	}
    }
    
    private void initView() {
    	tv_part_job=(TextView) rootView.findViewById(R.id.tv_part_job);
    	tv_full_time=(TextView) rootView.findViewById(R.id.tv_full_time);
    	lv_job=(ListView) rootView.findViewById(R.id.lv_job);
    	
    	tv_part_job.setOnClickListener(this);
    	tv_full_time.setOnClickListener(this);
    	
    	adapter=new JobAdapter(getActivity(), part_job_Data);
    	lv_job.setAdapter(adapter);
    }

	@Override
	public void onClick(View v) {
		setTextColor();
		switch (v.getId()) {
		case R.id.tv_part_job:
			tv_part_job.setTextColor(getResources().getColor(R.color.basecolor));
			job_type=0;
			adapter=new JobAdapter(getActivity(), part_job_Data);
	    	lv_job.setAdapter(adapter);
			break;
		case R.id.tv_full_time:	
			tv_full_time.setTextColor(getResources().getColor(R.color.basecolor));
			job_type=1;
			adapter=new JobAdapter(getActivity(), full_job_Data);
	    	lv_job.setAdapter(adapter);
			break;
		}
	}
	
	//�ı�����ı�����ɫ
	public void setTextColor() {
		tv_part_job.setTextColor(getResources().getColor(R.color.text_color_primary));
		tv_full_time.setTextColor(getResources().getColor(R.color.text_color_primary));
	}

}
