package com.example.onlinecareer.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.onlinecareer.R;

public class PersonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);
    }
}
