package com.example.onlinecareer.activities;


import android.app.Activity;
import android.view.KeyEvent;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.example.onlinecareer.R;
import com.example.onlinecareer.fragment.OneFragment;
import com.example.onlinecareer.fragment.ThreeFragment;
import com.example.onlinecareer.fragment.TwoFragment;
import com.example.onlinecareer.util.CommonData;

@SuppressLint("NewApi")
public class MainActivity extends Activity implements
		OnCheckedChangeListener {

	private FrameLayout frameLayout;
	public OneFragment oneFragment;
	public TwoFragment twoFragment;
	public ThreeFragment threeFragment;
	private RadioButton radioButton1, radioButton2, radioButton3;

	private int currentTab = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);


		CommonData.db = SQLiteDatabase.openOrCreateDatabase(this.getFilesDir().toString() + "/stu.db", null);

		createTable(CommonData.db);

		initView();
		initEvent();

	}


	private void createTable(SQLiteDatabase db) {

		if (tableIsExist(db,"user_table"))
			return;

		String stu_table = "create table user_table(_id integer primary key autoincrement," +
				"tel varchar(20)," +
				"pwd varchar(20)," +
				"name varchar(20)," +
				"sex varchar(20)," +
				"age varchar(20)," +
				"degrees varchar(50)," +
				"experience varchar(50),"+
				"phone varchar(20))";

		db.execSQL(stu_table);
	}

	public boolean tableIsExist(SQLiteDatabase db,String tableName) {
		boolean result = false;
		if (tableName == null) {
			return false;
		}
		try {
			String sql = "select count(*) as c from Sqlite_master  where type ='table' and name ='"+tableName.trim()+"' ";
			Cursor cursor = db.rawQuery(sql, null);
			if (cursor.moveToNext()) {
				int count = cursor.getInt(0);
				if (count > 0) {
					result = true;
				}
			}
			cursor.close();

		} catch (Exception e) {
		}
		return result;
	}
	//<<<<<<<<<<<<<<<<<<<<<<<���ݿ����<<<<<<<<<<<<<<<<<<<<<<<<<

	@Override
	protected void onResume() {
		super.onResume();
		setSelect(currentTab);
	}

	private void initView() {
		frameLayout = (FrameLayout) findViewById(R.id.fl_content);
		radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
		radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
		radioButton3 = (RadioButton) findViewById(R.id.radioButton3);
	}

	private void initEvent() {
		// TODO Auto-generated method stub
		radioButton1.setOnCheckedChangeListener(this);
		radioButton2.setOnCheckedChangeListener(this);
		radioButton3.setOnCheckedChangeListener(this);
		setSelect(0);
		radioButton1.setCompoundDrawablesWithIntrinsicBounds(
				null,
				getApplication().getResources().getDrawable(
						R.drawable.home_selected), null, null);
		radioButton1.setTextColor(0xff2C97D4);
	}

	private void setImg() {
		radioButton1.setCompoundDrawablesWithIntrinsicBounds(
				null,
				getApplication().getResources().getDrawable(
						R.drawable.home_normal), null, null);
		radioButton2.setCompoundDrawablesWithIntrinsicBounds(
				null,
				getApplication().getResources().getDrawable(
						R.drawable.message_normal), null, null);
		radioButton3.setCompoundDrawablesWithIntrinsicBounds(
				null,
				getApplication().getResources().getDrawable(
						R.drawable.my_normal), null, null);
		radioButton1.setTextColor(0xff979797);
		radioButton2.setTextColor(0xff979797);
		radioButton3.setTextColor(0xff979797);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			switch (buttonView.getId()) {
				case R.id.radioButton1:
					setImg();
					currentTab = 0;
					radioButton1.setTextColor(0xff2C97D4);
					radioButton1.setCompoundDrawablesWithIntrinsicBounds(
							null,
							getApplication().getResources().getDrawable(
									R.drawable.home_selected), null, null);
					setSelect(0);
					break;
				case R.id.radioButton2:
					if (CommonData.isLogin) {
						setImg();
						currentTab = 1;
						radioButton2.setTextColor(0xff2C97D4);
						radioButton2.setCompoundDrawablesWithIntrinsicBounds(
								null,
								getApplication().getResources().getDrawable(
										R.drawable.message_selected), null, null);
						setSelect(1);
					} else {
						Intent intent = new Intent(this, LoginActivity.class);
						startActivityForResult(intent, 1);
					}

					break;
				case R.id.radioButton3:
					setImg();
					currentTab = 2;
					radioButton3.setTextColor(0xff2C97D4);
					radioButton3.setCompoundDrawablesWithIntrinsicBounds(
							null,
							getApplication().getResources().getDrawable(
									R.drawable.my_selected), null, null);
					setSelect(2);
					break;
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String result = data.getExtras().getString("result");// �õ���Activity
		// �رպ󷵻ص�����
		if (result.equals("ok")) {
			setImg();
			currentTab = 1;
			radioButton2.setTextColor(0xff2C97D4);
			radioButton2.setCompoundDrawablesWithIntrinsicBounds(
					null,
					getApplication().getResources().getDrawable(
							R.drawable.message_selected), null, null);
			setSelect(1);
		} else {
			if (currentTab == 0) {
				radioButton1.setChecked(true);
				radioButton1.setTextColor(0xff2C97D4);
				radioButton1.setCompoundDrawablesWithIntrinsicBounds(
						null,
						getApplication().getResources().getDrawable(
								R.drawable.home_selected), null, null);
				setSelect(0);
			} else if (currentTab == 2) {
				radioButton3.setChecked(true);
				radioButton3.setTextColor(0xff2C97D4);
				radioButton3.setCompoundDrawablesWithIntrinsicBounds(
						null,
						getApplication().getResources().getDrawable(
								R.drawable.my_selected), null, null);
				setSelect(2);
			}
		}
	}

	public void setSelect(int i) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		switch (i) {
			case 0:
				hideFragment(transaction, twoFragment);
				hideFragment(transaction, threeFragment);
				if (oneFragment == null) {
					oneFragment = new OneFragment();
					transaction.add(R.id.fl_content, oneFragment, "oneFragment");
				} else {
					transaction.show(oneFragment);
				}
				break;
			case 1:
				hideFragment(transaction, oneFragment);
				hideFragment(transaction, threeFragment);
				if (twoFragment == null) {

					twoFragment = new TwoFragment();
					transaction.add(R.id.fl_content, twoFragment, "twoFragment");
				} else {
					transaction.show(twoFragment);
				}
				break;
			case 2:
				hideFragment(transaction, oneFragment);
				hideFragment(transaction, twoFragment);
				if (threeFragment == null) {
					threeFragment = new ThreeFragment();
					transaction
							.add(R.id.fl_content, threeFragment, "threeFragment");
				} else {
					transaction.show(threeFragment);
				}
				break;
		}
		transaction.commit();
	}

	private void hideFragment(FragmentTransaction transaction, Fragment fragment) {
		if (fragment != null) {
			transaction.hide(fragment);
		}
	}

	/**
	 * ����˳����˳������ʱ�򲻻�ر�,Ӧ���´ν����ʱ��������
	 */
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN
				&& event.getRepeatCount() == 0) {
			// ����Ĳ�������
			moveTaskToBack(true);
			return true;
		}
		return super.dispatchKeyEvent(event);
	}
}
