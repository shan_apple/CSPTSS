package com.example.onlinecareer.adapter;

import java.util.HashMap;
import java.util.List;


import com.example.onlinecareer.R;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class JobAdapter extends BaseAdapter {

	private List<HashMap<String, Object>> data;
	private Context context;

	public JobAdapter(Context context, List<HashMap<String, Object>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public HashMap<String, Object> getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = View.inflate(context, R.layout.item_job, null);
			new ViewHolder(convertView);
		}
		final ViewHolder holder = (ViewHolder) convertView.getTag();

		HashMap<String, Object> hashMap=getItem(position);
		
		holder.iv_job_icon.setBackgroundResource((Integer) hashMap.get("img"));
		holder.tv_job_name.setText(hashMap.get("name").toString());
		holder.tv_job_company.setText(hashMap.get("company").toString());
		holder.tv_job_time.setText(hashMap.get("time").toString());
		holder.tv_job_salary.setText(hashMap.get("salary").toString());
		holder.tv_job_type.setText(hashMap.get("jobType").toString());
		
		return convertView;
	}

	class ViewHolder {
		public ImageView iv_job_icon;
		public TextView tv_job_name;
		public TextView tv_job_company;
		public TextView tv_job_time;
		public TextView tv_job_salary;
		public TextView tv_job_type;

		public ViewHolder(View view) {
			iv_job_icon = (ImageView) view.findViewById(R.id.iv_job_icon);
			tv_job_name = (TextView) view.findViewById(R.id.tv_job_name);
			tv_job_company = (TextView) view.findViewById(R.id.tv_job_company);
			tv_job_time = (TextView) view.findViewById(R.id.tv_job_time);
			tv_job_salary = (TextView) view.findViewById(R.id.tv_job_salary);
			tv_job_type = (TextView) view.findViewById(R.id.tv_job_type);
			view.setTag(this);
		}
	}

}
