package com.example.demo1;

import android.content.Context;
import android.content.SharedPreferences;

public class User {
    private String name="";
    private String pwd="";
    private Boolean flag=false;

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    //存储文件名
    private  static  final  String SHARED_NAME="userInfo";




    //SharedPreferences方法
    //存储文件在data/data/com.example.demo1/shared_prefs
    SharedPreferences sharedPreferences;

    public  void saveUserBySharedPreferences(Context context) {

        sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name",name);
        editor.putString("pwd",pwd);
        editor.putBoolean("flag",flag);
        editor.commit();
    }

    public void getUserBySharedPreferences(Context context){
        sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        name=sharedPreferences.getString("name","");
        pwd=sharedPreferences.getString("pwd","");
        flag=sharedPreferences.getBoolean("flag",false);

    }






    public User() {
    }



    public User(String name, String pwd, Boolean flag) {
        this.name = name;
        this.pwd = pwd;
        this.flag=flag;

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }


}
