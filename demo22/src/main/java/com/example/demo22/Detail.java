package com.example.demo22;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.demo22.domain.Job;
import com.example.demo22.domain.User;
import com.example.demo22.service.UserService;
import com.example.demo22.web.Web;

public class Detail extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_back;
    private TextView name;
    private TextView zhi;
    private TextView company;
    private TextView balance;
    private TextView tv_people;
    private TextView tv_time;
    private TextView tv_add;
    private TextView tv_rusult;
    private TextView desc;
    private Button apply;


    String jobId;
    String jobType;
    User user;
    UserService userService;
    Web web;
    Job job = new Job();
    private ImageView image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent = getIntent();
        jobId = intent.getStringExtra("jobId");
        jobType = intent.getStringExtra("jobType");

        userService = new UserService(this);
        web = new Web(this);
        job = userService.getJianJob(jobId);
        initView();
        if (jobType.equals("jian")) {
            zhi.setText("兼职");
            image.setImageResource(R.drawable.photo1);
        } else if(jobType.equals("quan")) {
            zhi.setText("全职");
            image.setImageResource(R.drawable.photo2);
        }else {
            zhi.setText(jobType);
            zhi.setTextColor((Integer)R.color.red);
            image.setImageResource(R.drawable.photo2);
            apply.setVisibility(View.INVISIBLE);
        }

        name.setText(job.getName());

        company.setText(job.getCompany());
        balance.setText(job.getBalance());
        tv_people.setText(job.getPerson());
        tv_time.setText(job.getStarttime() + "-" + job.getEndtime());
        tv_add.setText(job.getPlace());
        tv_rusult.setText(job.getResult());
        desc.setText(job.getDesc());
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        name = (TextView) findViewById(R.id.name);
        zhi = (TextView) findViewById(R.id.zhi);
        company = (TextView) findViewById(R.id.company);
        balance = (TextView) findViewById(R.id.balance);
        tv_people = (TextView) findViewById(R.id.tv_people);
        tv_time = (TextView) findViewById(R.id.tv_time);
        tv_add = (TextView) findViewById(R.id.tv_add);
        tv_rusult = (TextView) findViewById(R.id.tv_rusult);
        desc = (TextView) findViewById(R.id.desc);
        apply = (Button) findViewById(R.id.apply);

        apply.setOnClickListener(this);
        tv_back.setOnClickListener(this);

        image = (ImageView) findViewById(R.id.image);
        image.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.apply:
                if (userService.addJob(user.getId(), jobId, jobType)) {
                    Toast.makeText(this, "报名成功", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "已报名该工作", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tv_back:
                this.finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        Log.i("tag", "onResume");
        user = userService.getUserBySharedPreferences();
        user = userService.getUserByPhone(user.getPhone());
        super.onResume();
    }
}
