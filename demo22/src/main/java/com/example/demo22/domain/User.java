package com.example.demo22.domain;

public class User {
    private String id;
    private String phone;
    private String password;
    private String username;
    private String sex;
    private String age;
    private String education;
    private String work;

    public User() {
    }

    public User(String phone, String username, String sex, String age, String education, String work) {
        this.phone = phone;

        this.username = username;
        this.sex = sex;
        this.age = age;
        this.education = education;
        this.work = work;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", username='" + username + '\'' +
                ", sex='" + sex + '\'' +
                ", age='" + age + '\'' +
                ", education='" + education + '\'' +
                ", work='" + work + '\'' +
                '}';
    }

    public User(String id, String phone, String password, String username, String sex, String age, String education, String work) {
        this.id = id;
        this.phone = phone;
        this.password = password;
        this.username = username;
        this.sex = sex;
        this.age = age;
        this.education = education;
        this.work = work;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }



}
