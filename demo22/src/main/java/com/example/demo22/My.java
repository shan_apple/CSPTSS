package com.example.demo22;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demo22.domain.User;
import com.example.demo22.service.UserService;
import com.example.demo22.web.Web;

public class My extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout tv_info;
    private LinearLayout tv_produce;
    private LinearLayout tv_logout;
    private TextView bottom_home;
    private TextView bottom_message;
    private TextView bottom_my;

    User user;


    Web web = null;
    UserService userService;
    private TextView name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        web = new Web(this);
        userService = new UserService(this);
        user = userService.getUserBySharedPreferences();
        user = userService.getUserByPhone(user.getPhone());
        initView();



    }

    private void initView() {
        tv_info = findViewById(R.id.tv_info);
        tv_produce = findViewById(R.id.tv_produce);
        tv_logout = findViewById(R.id.tv_logout);
        bottom_home = (TextView) findViewById(R.id.bottom_home);
        bottom_message = (TextView) findViewById(R.id.bottom_message);
        bottom_my = (TextView) findViewById(R.id.bottom_my);

        bottom_home.setOnClickListener(this);
        bottom_message.setOnClickListener(this);
        bottom_my.setOnClickListener(this);
        tv_info.setOnClickListener(this);
        tv_produce.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        name = (TextView) findViewById(R.id.name);
        name.setText(user.getUsername());
        name.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.bottom_home:
                intent = new Intent(this, Home.class);
                startActivity(intent);
                break;
            case R.id.bottom_message:
                intent = new Intent(this, Message.class);
                startActivity(intent);
                break;
            case R.id.bottom_my:
                intent = new Intent(this, My.class);
                startActivity(intent);
                break;
            case R.id.tv_info:

                intent = new Intent(this, UserInfo.class);
                startActivity(intent);
                break;
            case R.id.tv_produce:
                Toast.makeText(this, "此产品性能良好", Toast.LENGTH_SHORT).show();
                showNormalDialog();
                break;
            case R.id.tv_logout:
                User user1 = new User();
                userService.saveUserBySharedPreferences(user1);
                intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
        }


    }

    public void showNormalDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("产品介绍");
        builder.setMessage("随着信息技术的不断创新，" +
                "信息化校园建设的不断深入，" +
                "带动了移动应用的快速发展。" +
                "为了更好的服务于全校师生，" +
                "现推出iSufe应用，" +
                "现已良好的支持iPhone和Android终端，" +
                "在移动互联网高速发展的今天，" +
                "iSufe将会在未来不断的完善和充实，" +
                "为全校师生的工作和生活提供更为贴切的服务");
        builder.setPositiveButton("继续努力", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();//对话框消失
            }
        });
        builder.setNegativeButton("五星好评", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
