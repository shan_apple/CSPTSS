package com.example.demo22;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demo22.domain.User;
import com.example.demo22.service.UserService;
import com.example.demo22.web.Web;

public class UserInfo extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_back;
    private TextView tv_edit;
    private TextView tv_name;
    private TextView tv_sex;
    private TextView tv_age;
    private TextView tv_education;
    private TextView tv_work;
    private TextView tv_phone;

    UserService userService;
    Web web;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        initView();
        userService=new UserService(this);
        web=new Web(this);
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_edit = (TextView) findViewById(R.id.tv_edit);
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_sex = (TextView) findViewById(R.id.tv_sex);
        tv_age = (TextView) findViewById(R.id.tv_age);
        tv_education = (TextView) findViewById(R.id.tv_education);
        tv_work = (TextView) findViewById(R.id.tv_work);
        tv_phone = (TextView) findViewById(R.id.tv_phone);


        tv_back.setOnClickListener(this);
        tv_edit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                this.finish();
                break;
            case R.id.tv_edit:
                Intent intent=new Intent(this,ChangeInfo.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onResume() {
        Log.i("tag", "onResume");
        user = userService.getUserBySharedPreferences();
        user=userService.getUserByPhone(user.getPhone());
        if(user.getPhone()==null&&user.getPhone().length()==0){
            //未登录
        }else {
            user=web.getUserByPhone(user.getPhone());
            tv_name.setText(user.getUsername());
            tv_sex.setText(user.getSex());
            tv_age.setText(user.getAge());
            tv_education.setText(user.getEducation());
            tv_work.setText(user.getWork());
            tv_phone.setText(user.getPhone());
        }

        super.onResume();
    }

}
