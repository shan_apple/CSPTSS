package com.example.demo22;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demo22.domain.Job;
import com.example.demo22.domain.User;
import com.example.demo22.service.UserService;
import com.example.demo22.web.Web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_title;
    private View view_jian;
    private View view_quan;
    private TextView bottom_home;
    private TextView bottom_message;
    private TextView bottom_my;
    private ListView joblist_jian;
    private LinearLayout linearLayout_jian;
    private ListView joblist_quan;
    private LinearLayout linearLayout_quan;

    Web web = null;
    UserService userService;
    User user;
    List<Job> jianlist;
    List<Job> quanlist;
    private TextView tv_jian;
    private TextView tv_quan;

    public Home() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        web = new Web(this);
        userService = new UserService(this);
        initView();


    }

    private void initView() {

        tv_title = (TextView) findViewById(R.id.tv_title);
        view_jian = (View) findViewById(R.id.view_jian);
        view_quan = (View) findViewById(R.id.view_quan);
        bottom_home = (TextView) findViewById(R.id.bottom_home);
        bottom_message = (TextView) findViewById(R.id.bottom_message);
        bottom_my = (TextView) findViewById(R.id.bottom_my);

        bottom_home.setOnClickListener(this);
        bottom_message.setOnClickListener(this);
        bottom_my.setOnClickListener(this);

        joblist_jian = (ListView) findViewById(R.id.joblist_jian);
        linearLayout_jian = (LinearLayout) findViewById(R.id.linearLayout_jian);
        joblist_quan = (ListView) findViewById(R.id.joblist_quan);
        linearLayout_quan = (LinearLayout) findViewById(R.id.linearLayout_quan);



        List<Map<String, Object>> list = getdata();
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, list,
                R.layout.job,
                new String[]{"image", "name", "zhi", "company", "startTime", "endTime", "balance"},
                new int[]{R.id.image, R.id.name, R.id.zhi, R.id.company, R.id.startTime, R.id.endTime, R.id.balance});
        joblist_jian.setAdapter(simpleAdapter);
        joblist_jian.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Home.this, Detail.class);
                intent.putExtra("jobId", jianlist.get(position).getId());
                intent.putExtra("jobType", "jian");
                startActivity(intent);
            }
        });

        List<Map<String, Object>> list_quan = getdataQuan();
        SimpleAdapter simpleAdapter_quan = new SimpleAdapter(this, list_quan,
                R.layout.job,
                new String[]{"image", "name", "zhi", "company", "startTime", "endTime", "balance"},
                new int[]{R.id.image, R.id.name, R.id.zhi, R.id.company, R.id.startTime, R.id.endTime, R.id.balance});
        joblist_quan.setAdapter(simpleAdapter_quan);
        joblist_quan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Home.this, Detail.class);
                intent.putExtra("jobId", quanlist.get(position).getId());
                intent.putExtra("jobType", "quan");
                startActivity(intent);
            }
        });
        tv_jian = (TextView) findViewById(R.id.tv_jian);
        tv_jian.setOnClickListener(this);
        tv_quan = (TextView) findViewById(R.id.tv_quan);
        tv_quan.setOnClickListener(this);
    }

    public List<Map<String, Object>> getdata() {


        jianlist = userService.getJianJobs();

        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < jianlist.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo1);
            map.put("name", jianlist.get(i).getName());
            map.put("zhi", "兼职");
            map.put("company", jianlist.get(i).getCompany());
            map.put("startTime", jianlist.get(i).getStarttime()+"-"+jianlist.get(i).getEndtime());
            map.put("endTime","");
            map.put("balance", jianlist.get(i).getBalance());
            list.add(map);
        }
        return list;
    }

    public List<Map<String, Object>> getdataQuan() {


        quanlist = userService.getQuanJobs();

        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < quanlist.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo2);
            map.put("name", quanlist.get(i).getName());
            map.put("zhi", "全职");
            map.put("company", quanlist.get(i).getCompany());
            map.put("startTime", quanlist.get(i).getStarttime()+"-"+quanlist.get(i).getEndtime());
            map.put("endTime", "");
            map.put("balance", quanlist.get(i).getBalance());
            list.add(map);
        }
        return list;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.bottom_home:
                //intent=new Intent(this,Home.class);
                //startActivity(intent);
                break;
            case R.id.bottom_message:
                intent=new Intent(this,Message.class);
                startActivity(intent);
                break;
            case R.id.bottom_my:
                intent = new Intent(this, My.class);
                intent.putExtra("userPhone", user.getPhone());
                startActivity(intent);
                break;
            case R.id.tv_jian:
                Toast.makeText(this, "兼职", Toast.LENGTH_SHORT).show();
                view_jian.setVisibility(linearLayout_jian.VISIBLE);
                view_quan.setVisibility(linearLayout_quan.INVISIBLE);
                linearLayout_jian.setVisibility(linearLayout_jian.VISIBLE);
                linearLayout_quan.setVisibility(linearLayout_quan.GONE);



                break;
            case R.id.tv_quan:
                Toast.makeText(this, "全职", Toast.LENGTH_SHORT).show();
                view_jian.setVisibility(linearLayout_jian.INVISIBLE);
                view_quan.setVisibility(linearLayout_quan.VISIBLE);
                linearLayout_jian.setVisibility(linearLayout_jian.GONE);
                linearLayout_quan.setVisibility(linearLayout_quan.VISIBLE);
                break;
        }
    }

    @Override
    protected void onResume() {
        Log.i("tag", "onResume");
        user = userService.getUserBySharedPreferences();
        user = userService.getUserByPhone(user.getPhone());
        super.onResume();
    }
}
