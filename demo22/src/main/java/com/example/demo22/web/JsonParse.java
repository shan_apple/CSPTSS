package com.example.demo22.web;

import com.example.demo22.domain.Job;
import com.example.demo22.domain.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class JsonParse {

    public static List<User> getUsers(String json) {
        //使用gson库解析JSON数据
        Gson gson = new Gson();
        //创建一个TypeToken的匿名子类对象，并调用对象的getType()方法
        Type listType = new TypeToken<List<User>>() { }.getType();
        //把获取到的信息集合存到newsInfos中
        List<User> newsInfos = gson.fromJson(json, listType);
        return newsInfos;
    }

    public static User getUser(String json) {
        //使用gson库解析JSON数据
        Gson gson = new Gson();
        //创建一个TypeToken的匿名子类对象，并调用对象的getType()方法
        Type listType = new TypeToken<User>() { }.getType();
        //把获取到的信息集合存到newsInfos中
        User user = gson.fromJson(json, listType);
        return user;
    }

    public static Job getJob(String json) {
        //使用gson库解析JSON数据
        Gson gson = new Gson();
        //创建一个TypeToken的匿名子类对象，并调用对象的getType()方法
        Type listType = new TypeToken<Job>() { }.getType();
        //把获取到的信息集合存到newsInfos中
        Job job = gson.fromJson(json, listType);
        return job;
    }


    public static List<Job> getJobs(String json) {
        //使用gson库解析JSON数据
        Gson gson = new Gson();
        //创建一个TypeToken的匿名子类对象，并调用对象的getType()方法
        Type listType = new TypeToken<List<Job>>() { }.getType();
        //把获取到的信息集合存到newsInfos中
        List<Job> newsInfos = gson.fromJson(json, listType);
        return newsInfos;
    }
}
