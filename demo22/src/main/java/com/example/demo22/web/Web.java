package com.example.demo22.web;
import android.content.Context;
import android.util.Log;

import com.example.demo22.domain.Job;
import com.example.demo22.domain.User;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Web {
    private Context context;
    public Web(Context context){
        this.context=context;
    }

    public User getUserByPhone(String phone){
        MyThread myThread=new MyThread(context,"getUserByPhone/"+phone);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        //User user=JsonParse.getUser("{\"age\":16,\"education\":\"洛阳理工学院\",\"id\":2,\"password\":\"123\",\"phone\":123,\"sex\":\"女\",\"username\":\"王二\",\"work\":\"学生\"}");
        User user=JsonParse.getUser(myThread.getMsgJson());
        System.out.println("USER   list"+user);
        return user;

    }

    public Boolean register(String phone,String password){
        MyThread myThread=new MyThread(context,"register/"+phone+"/"+password);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(myThread.getMsgJson().contains("0")){
            return false;
        }else {
            return true;
        }
    }

    public void changeInfo(User user){
        MyThread myThread=new MyThread(context,
                "changeInfo/"+user.getId()+"/"+user.getUsername()
                        +"/"+user.getSex()+"/"+user.getAge()+"/"+user.getEducation()+"/"+user.getWork());
        myThread.start();
    }

    public List<Job> getQuanJobs(){
        List<Job> jobs=new ArrayList<>();
        MyThread myThread=new MyThread(context,"getQuanJobs");
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());

        return jobs;
    }
    public List<Job> getJianJobs(){
        List<Job> jobs;
        MyThread myThread=new MyThread(context,"getJianJobs");
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(myThread.getMsgJson());
        jobs=JsonParse.getJobs(myThread.getMsgJson());
        System.out.println(jobs.get(0));
        return jobs;
    }
    public List<Job> getQuanJobs3(String id){
        List<Job> jobs=new ArrayList<>();
        MyThread myThread=new MyThread(context,"getQuanJobs3ById"+"/"+id);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());
        /*jobs=JsonParse.getJobs("[{\"balance\":100每天,\"desc\":\"实习\"," +
                "\"endtime\":5.3,\"id\":1,\"name\":\"洛理\",\"place\":" +
                "\"洛理\",\"starttime\":4.01,\"company\":公司}]");*/
        return jobs;
    }
    public List<Job> getQuanJobs2(String id){
        List<Job> jobs=new ArrayList<>();
        MyThread myThread=new MyThread(context,"getQuanJobs2ById/"+id);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());
        /*jobs=JsonParse.getJobs("[{\"balance\":100每天,\"desc\":\"实习\"," +
                "\"endtime\":5.3,\"id\":1,\"name\":\"洛理\",\"place\":" +
                "\"洛理\",\"starttime\":4.01,\"company\":公司}]");*/
        return jobs;
    }
    public List<Job> getQuanJobs1(String id){
        List<Job> jobs=new ArrayList<>();
        MyThread myThread=new MyThread(context,"getQuanJobs1ById/"+id);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());
        /*jobs=JsonParse.getJobs("[{\"balance\":100每天,\"desc\":\"实习\"," +
                "\"endtime\":5.3,\"id\":1,\"name\":\"洛理\",\"place\":" +
                "\"洛理\",\"starttime\":4.01,\"company\":公司}]");*/
        return jobs;
    }


    public List<Job> getJianJobs3(String id){
        List<Job> jobs;
        MyThread myThread=new MyThread(context,"getJianJobs3ById/"+id);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());
        return jobs;
    }
    public List<Job> getJianJobs2(String id){
        List<Job> jobs;
        MyThread myThread=new MyThread(context,"getJianJobs2ById/"+id);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());

        return jobs;
    }
    public List<Job> getJianJobs1(String id){
        List<Job> jobs;
        MyThread myThread=new MyThread(context,"getJianJobs1ById/"+id);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        jobs=JsonParse.getJobs(myThread.getMsgJson());

        return jobs;
    }

    public Job getJianJob(String jobId){
        Job job;
        MyThread myThread=new MyThread(context,"getQuanJobById/"+jobId);
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        job=JsonParse.getJob(myThread.getMsgJson());
        return job;
    }


    public Boolean addJob(String userId,String jobId,String jobType){
        MyThread myThread;
        if(jobType.equals("jian")){
            myThread=new MyThread(context,"addJianJob/"+userId+"/"+jobId);
        }else {
            myThread=new MyThread(context,"addQuanJob/"+userId+"/"+jobId);
        }
        myThread.start();
        while (myThread.getMsgJson() == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(myThread.getMsgJson().contains("0")){
            return false;
        }else {
            return true;
        }

    }










}
