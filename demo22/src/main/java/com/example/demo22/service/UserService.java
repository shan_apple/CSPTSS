package com.example.demo22.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.demo22.domain.Job;
import com.example.demo22.domain.User;
import com.example.demo22.web.Web;

import java.util.ArrayList;
import java.util.List;

public class UserService {
//存储文件名
private  static  final  String SHARED_NAME="userInfo";
    SharedPreferences sharedPreferences;
    private Context context;
    Web web;

    public UserService(Context context){
        this.context=context;
        web=new Web(context);
    }

    public  void saveUserBySharedPreferences( User user) {
        sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id",user.getId());
        editor.putString("phone",user.getPhone());
        editor.putString("pwd",user.getPassword());
        editor.commit();
    }

    public User getUserBySharedPreferences(){
        sharedPreferences = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
        User user=new User();
        user.setId(sharedPreferences.getString("id"," "));
        user.setPhone(sharedPreferences.getString("phone"," "));;
        user.setPassword(sharedPreferences.getString("pwd"," "));
        return user;
    }

    //注册用户
    public Boolean register(String phone,String password){
        return web.register(phone,password);
    }

    //更改信息
    public void changeInfo(User user){
        web.changeInfo(user);
    }


    public List<Job> getJianJobs(){
        List<Job> jobs=web.getJianJobs();
        return jobs;
    }
    public List<Job> getJianJobs3(String id){
        List<Job> jobs=web.getJianJobs3(id);
        return jobs;
    }
    public List<Job> getJianJobs2(String id){
        List<Job> jobs=web.getJianJobs2(id);
        return jobs;
    }
    public List<Job> getJianJobs1(String id){
        List<Job> jobs=web.getJianJobs1(id);
        return jobs;
    }
    public List<Job> getQuanJobs(){
        List<Job> jobs=web.getQuanJobs();
        return jobs;
    }
    public List<Job> getQuanJobs3(String id){
        List<Job> jobs=web.getQuanJobs3(id);
        return jobs;
    }
    public List<Job> getQuanJobs2(String id){
        List<Job> jobs=web.getQuanJobs2(id);
        return jobs;
    }
    public List<Job> getQuanJobs1(String id){
        List<Job> jobs=web.getQuanJobs1(id);
        return jobs;
    }

    public Job getJianJob(String jobId){
        return web.getJianJob(jobId);
    }

    public Boolean addJob(String userId,String jobId,String jobType){
        return web.addJob(userId,jobId,jobType);
    }

    public User getUserByPhone(String phone){
        return web.getUserByPhone(phone);
    }






}
