package com.example.demo22;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demo22.domain.User;
import com.example.demo22.service.UserService;
import com.example.demo22.web.Web;

public class ChangeInfo extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_back;
    private TextView tv_save;
    private EditText et_name;
    private RadioButton radio_man;
    private RadioButton radio_wuman;
    private RadioGroup radio_group;
    private EditText ed_age;
    private EditText ed_education;
    private EditText ed_work;
    private EditText et_phone;

    Web web = null;
    UserService userService;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_ingo);
        initView();
        web = new Web(this);
        userService = new UserService(this);
    }

    private void initView() {
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_save = (TextView) findViewById(R.id.tv_save);
        et_name = (EditText) findViewById(R.id.et_name);
        radio_man = (RadioButton) findViewById(R.id.radio_man);
        radio_wuman = (RadioButton) findViewById(R.id.radio_wuman);
        radio_group = (RadioGroup) findViewById(R.id.radio_group);
        ed_age = (EditText) findViewById(R.id.ed_age);
        ed_education = (EditText) findViewById(R.id.ed_education);
        ed_work = (EditText) findViewById(R.id.ed_work);
        et_phone = (EditText) findViewById(R.id.et_phone);


        tv_back.setOnClickListener(this);
        tv_save.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_back:
                this.finish();
                break;
            case R.id.tv_save:
                submit();
                break;
        }
    }

    private void submit() {
        // validate
        String name = et_name.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "name不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        String age = ed_age.getText().toString().trim();
        if (TextUtils.isEmpty(age)) {
            Toast.makeText(this, "age不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        String education = ed_education.getText().toString().trim();
        if (TextUtils.isEmpty(education)) {
            Toast.makeText(this, "education不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        String work = ed_work.getText().toString().trim();
        if (TextUtils.isEmpty(work)) {
            Toast.makeText(this, "work不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        String phone = et_phone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "phone不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        String sex = "";
        int count = radio_group.getChildCount();
        for (int i = 0; i < count; i++) {
            RadioButton radioButton = (RadioButton) radio_group.getChildAt(i);
            if (radioButton.isChecked()) {
                sex = radioButton.getText().toString();

                break;
            }
        }
        if(sex.length()<=0){
            Toast.makeText(this, "性别不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        user.setUsername(name);
        user.setPhone(phone);
        user.setSex(sex);
        user.setAge(age);
        user.setEducation(education);
        user.setWork(work);
        userService.changeInfo(user);

        //保存到本地
        userService.saveUserBySharedPreferences(user);
        userService.changeInfo(user);
        Toast.makeText(this, "修改成功", Toast.LENGTH_SHORT).show();


    }

    @Override
    protected void onResume() {
        Log.i("tag", "onResume");

        user = userService.getUserBySharedPreferences();
        user=userService.getUserByPhone(user.getPhone());
        if(user.getPhone()==null&&user.getPhone().length()==0){
            //未登录
        }else {
            user=web.getUserByPhone(user.getPhone());
            et_name.setText(user.getUsername());
            if(user.getSex().equals("男")){
                radio_man.setChecked(true);
            }else {
                radio_wuman.setChecked(true);
            }
            ed_age.setText(user.getAge());
            ed_education.setText(user.getEducation());
            ed_work.setText(user.getWork());
            et_phone.setText(user.getPhone());
        }

        super.onResume();
    }
}
