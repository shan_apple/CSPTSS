package com.example.online1.web;

import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import okhttp3.Request;

public class MyThread extends Thread{
    private  RequestQueue queue;
    private  Request request;
    private  String msgJson;
    private Context context;
    private String path;


    @Override
    public String toString() {
        return "MyThread{" +
                "queue=" + queue +
                ", request=" + request +
                ", msgJson='" + msgJson + '\'' +
                ", context=" + context +
                ", path='" + path + '\'' +
                '}';
    }

    public String getMsgJson() {
        return msgJson;
    }

    public void setMsgJson(String msgJson) {
        this.msgJson = msgJson;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MyThread(Context context, String path) {

        this.context = context;
        Log.i("context","005"+this.context.toString());
        this.path = path;
        this.queue= Volley.newRequestQueue(context);
    }





    public void run() {
        try {
            URL url = new URL(Common.PATH +path);  //创建URL对象
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置请求的方式
            conn.setRequestMethod("GET");
            //设置超时时间
            conn.setConnectTimeout(5000);
            int code = conn.getResponseCode();
            if (code == 200) {
                InputStream is = conn.getInputStream();
                ByteArrayOutputStream outStream = new ByteArrayOutputStream();
                byte[] data = new byte[1024];
                int len = 0;
                InputStream inStream = conn.getInputStream();
                while ((len = inStream.read(data)) != -1) {
                    outStream.write(data, 0, len);
                }
                inStream.close();
                String msgJson1=new String(outStream.toByteArray());
                //System.out.println("msgJson"+msgJson1);
                this.msgJson=msgJson1;

            } else {

            }
        } catch (Exception e) {
        }

    }




}
