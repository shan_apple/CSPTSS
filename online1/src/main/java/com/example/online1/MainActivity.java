package com.example.online1;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.online1.domain.User;
import com.example.online1.service.UserService;
import com.example.online1.web.Web;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText edit_phone;
    private EditText edit_password;
    private CheckBox checkBox2;
    private Button button_logi;

    Web web = null;
    UserService userService;
    private TextView tv_register;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        web = new Web(this);
        Log.i("context",this.toString());
        userService = new UserService(this);
        System.out.println("#######################"+userService.getUserByPhone("123456"));


    }


    private void initView() {
        edit_phone = (EditText) findViewById(R.id.edit_phone);
        edit_password = (EditText) findViewById(R.id.edit_password);
        checkBox2 = (CheckBox) findViewById(R.id.checkBox2);
        button_logi = (Button) findViewById(R.id.button_logi);

        button_logi.setOnClickListener(this);


        tv_register = (TextView) findViewById(R.id.tv_register);
        tv_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.button_logi:
                submit();

                break;
            case R.id.tv_register:
                intent=new Intent(this,Register.class);
                startActivity(intent);
                break;
        }
    }

    private void submit() {
        // validate
        String username = edit_phone.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            Toast.makeText(this, "username不能为空", Toast.LENGTH_SHORT).show();
            return;
        }

        String password = edit_password.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "password不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        User user2 = web.getUserByPhone(username);
        if (user2.getPhone() == null || user2.getPhone().length() < 1) {
            Toast.makeText(this, "用户未注册", Toast.LENGTH_SHORT).show();
            return;
        } else {
            if (user2.getPassword().equals(password)) {
                Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "密码错误", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        Boolean flag = checkBox2.isChecked();
        User user;
        if (flag) {
            user = new User();
            user.setId(user2.getId());
            user.setPhone(username);
            user.setPassword(password);
        } else {
            user = new User();
        }
        userService.saveUserBySharedPreferences(user);
        Intent intent;
        intent=new Intent(this,Home.class);
        startActivity(intent);


    }

    @Override
    protected void onResume() {
        Log.i("tag", "onResume");
        User user ;
        //SharedPreferences方式自动填写
        user = userService.getUserBySharedPreferences();
        edit_phone.setText(user.getPhone());
        edit_password.setText(user.getPassword());
        checkBox2.setChecked(user.getPhone().length() > 0);
        Toast.makeText(this, "自动登录", Toast.LENGTH_SHORT).show();
        if(user.getId()!=null) submit();
        super.onResume();
    }
}

