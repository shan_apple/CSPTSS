package com.example.online1;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.online1.domain.Job;
import com.example.online1.domain.User;
import com.example.online1.service.UserService;
import com.example.online1.web.Web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Message extends AppCompatActivity implements View.OnClickListener{
    private TextView tv_title;
    private View view_jian;
    private View view_quan;
    private TextView bottom_home;
    private TextView bottom_message;
    private TextView bottom_my;
    private ListView joblist_jian;
    private LinearLayout linearLayout_jian;
    private ListView joblist_quan;
    private LinearLayout linearLayout_quan;

    Web web = null;
    UserService userService;
    User user;
    List<Job> jianlist;
    List<Job> quanlist;
    private TextView tv_jian;
    private TextView tv_quan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        web = new Web(this);
        userService = new UserService(this);
        user = userService.getUserBySharedPreferences();
        user = userService.getUserByPhone(user.getPhone());
        initView();
    }
    private void initView() {


        tv_title = (TextView) findViewById(R.id.tv_title);
        view_jian = (View) findViewById(R.id.view_jian);
        view_quan = (View) findViewById(R.id.view_quan);
        bottom_home = (TextView) findViewById(R.id.bottom_home);
        bottom_message = (TextView) findViewById(R.id.bottom_message);
        bottom_my = (TextView) findViewById(R.id.bottom_my);

        bottom_home.setOnClickListener(this);
        bottom_message.setOnClickListener(this);
        bottom_my.setOnClickListener(this);

        joblist_jian = (ListView) findViewById(R.id.joblist_jian);
        linearLayout_jian = (LinearLayout) findViewById(R.id.linearLayout_jian);
        joblist_quan = (ListView) findViewById(R.id.joblist_quan);
        linearLayout_quan = (LinearLayout) findViewById(R.id.linearLayout_quan);



        final List<Map<String, Object>> list = getdata();
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, list,
                R.layout.job,
                new String[]{"image", "name", "zhi", "company", "startTime", "endTime", "balance"},
                new int[]{R.id.image, R.id.name, R.id.zhi, R.id.company, R.id.startTime, R.id.endTime, R.id.balance});
        joblist_jian.setAdapter(simpleAdapter);
        joblist_jian.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Message.this, Detail.class);
                intent.putExtra("jobId", jianlist.get(position).getId());
                intent.putExtra("jobType", (String) list.get(position).get("zhi"));
                startActivity(intent);
            }
        });

        final List<Map<String, Object>> list_quan = getdataQuan();
        SimpleAdapter simpleAdapter_quan = new SimpleAdapter(this, list_quan,
                R.layout.job,
                new String[]{"image", "name", "zhi", "company", "startTime", "endTime", "balance"},
                new int[]{R.id.image, R.id.name, R.id.zhi, R.id.company, R.id.startTime, R.id.endTime, R.id.balance});
        joblist_quan.setAdapter(simpleAdapter_quan);
        joblist_quan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Message.this, Detail.class);
                intent.putExtra("jobId", quanlist.get(position).getId());
                intent.putExtra("jobType",(String) list_quan.get(position).get("zhi"));
                startActivity(intent);
            }
        });
        tv_jian = (TextView) findViewById(R.id.tv_jian);
        tv_jian.setOnClickListener(this);
        tv_quan = (TextView) findViewById(R.id.tv_quan);
        tv_quan.setOnClickListener(this);
    }

    //兼职
    public List<Map<String, Object>> getdata() {
        jianlist=userService.getJianJobs3(user.getId());
        List<Job> jianlist3=userService.getJianJobs3(user.getId());
        List<Job> jianlist2=userService.getJianJobs2(user.getId());
        List<Job> jianlist1=userService.getJianJobs1(user.getId());
        jianlist.addAll(jianlist2);
        jianlist.addAll(jianlist1);

        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < jianlist3.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo1);
            map.put("name", jianlist3.get(i).getName());
            map.put("zhi", "已录用");
            map.put("company", jianlist3.get(i).getCompany());
            map.put("startTime", jianlist3.get(i).getStarttime());
            map.put("endTime", jianlist3.get(i).getEndtime());
            map.put("balance", jianlist3.get(i).getBalance());
            list.add(map);
        }
        for (int i = 0; i < jianlist2.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo1);
            map.put("name", jianlist2.get(i).getName());
            map.put("zhi", "待录用");
            map.put("company", jianlist2.get(i).getCompany());
            map.put("startTime", jianlist2.get(i).getStarttime());
            map.put("endTime", jianlist2.get(i).getEndtime());
            map.put("balance", jianlist2.get(i).getBalance());
            list.add(map);
        }
        for (int i = 0; i < jianlist1.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo1);
            map.put("name", jianlist1.get(i).getName());
            map.put("zhi", "已报名");
            map.put("company", jianlist1.get(i).getCompany());
            map.put("startTime", jianlist1.get(i).getStarttime());
            map.put("endTime", jianlist1.get(i).getEndtime());
            map.put("balance", jianlist1.get(i).getBalance());
            list.add(map);
        }
        return list;
    }

    public List<Map<String, Object>> getdataQuan() {
        quanlist=userService.getJianJobs3(user.getId());
        List<Job> quanlist3=userService.getJianJobs3(user.getId());
        List<Job> quanlist2=userService.getJianJobs2(user.getId());
        List<Job> quanlist1=userService.getJianJobs1(user.getId());
        quanlist.addAll(quanlist2);
        quanlist.addAll(quanlist1);



        List<Map<String, Object>> list = new ArrayList<>();

        for (int i = 0; i < quanlist3.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo2);
            map.put("name", quanlist3.get(i).getName());
            map.put("zhi", "已录用");
            map.put("company", quanlist3.get(i).getCompany());
            map.put("startTime", quanlist3.get(i).getStarttime());
            map.put("endTime", quanlist3.get(i).getEndtime());
            map.put("balance", quanlist3.get(i).getBalance());
            list.add(map);
        }
        for (int i = 0; i < quanlist2.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo2);
            map.put("name", quanlist2.get(i).getName());
            map.put("zhi", "待录用");
            map.put("company", quanlist2.get(i).getCompany());
            map.put("startTime", quanlist2.get(i).getStarttime());
            map.put("endTime", quanlist2.get(i).getEndtime());
            map.put("balance", quanlist2.get(i).getBalance());
            list.add(map);
        }
        for (int i = 0; i < quanlist1.size(); i++) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", R.drawable.photo2);
            map.put("name", quanlist1.get(i).getName());
            map.put("zhi", "已报名");
            map.put("company", quanlist1.get(i).getCompany());
            map.put("startTime", quanlist1.get(i).getStarttime());
            map.put("endTime", quanlist1.get(i).getEndtime());
            map.put("balance", quanlist1.get(i).getBalance());
            list.add(map);
        }
        return list;
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.bottom_home:
                intent=new Intent(this,Home.class);
                startActivity(intent);
                break;
            case R.id.bottom_message:
                //intent=new Intent(this,Message.class);
                //startActivity(intent);
                break;
            case R.id.bottom_my:
                intent = new Intent(this, My.class);
                intent.putExtra("userPhone", user.getPhone());
                startActivity(intent);
                break;
            case R.id.tv_jian:
                Toast.makeText(this, "兼职", Toast.LENGTH_SHORT).show();
                view_jian.setVisibility(linearLayout_jian.VISIBLE);
                view_quan.setVisibility(linearLayout_quan.INVISIBLE);
                linearLayout_jian.setVisibility(linearLayout_jian.VISIBLE);
                linearLayout_quan.setVisibility(linearLayout_quan.GONE);



                break;
            case R.id.tv_quan:
                Toast.makeText(this, "全职", Toast.LENGTH_SHORT).show();
                view_jian.setVisibility(linearLayout_jian.INVISIBLE);
                view_quan.setVisibility(linearLayout_quan.VISIBLE);
                linearLayout_jian.setVisibility(linearLayout_jian.GONE);
                linearLayout_quan.setVisibility(linearLayout_quan.VISIBLE);
                break;
        }
    }

    @Override
    protected void onResume() {
        Log.i("tag", "onResume");

        super.onResume();
    }
}

